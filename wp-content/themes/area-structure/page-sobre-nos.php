<?php get_header() ?>
    <section class="thumb-page" style="background-image: url(<?= the_post_thumbnail_url() ?>);">
        <div class="al-container">
            <div class="local"><a href="<?= get_home_url() ?>">Home</a>  <span>|</span>  Sobre Nós</div>
            <div class="title"><?= the_title() ?></div>
        </div>
    </section>
    <section class="texto-sobre">
        <div class="al-container">
            <?= the_content()?>
        </div>
    </section>
    <section class="left-right-faq espaco">
        <div class="al-container">
            <div class="left">
                <h2 class="title">Vejas as principais dúvidas dos pacientes</h2>
            </div>
            <div class="right">
                <ul class="faq">
                    <?php foreach(get_field("faq") as $chave => $faq): ?>
                        <li class="faqli" onclick="auto(<?= $chave ?>)">
                            <div class="back">
                                <div class="line" id="line-<?= $chave ?>">
                                    <?= $faq['titulo'] ?>
                                    <div class="img">
                                        <svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg" class="down" id="down-<?= $chave ?>">
                                            <path d="M1 1L9 9L17 1" stroke="#0054CE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg" class="up" id="up-<?= $chave ?>">
                                            <path d="M17 9L9 1L1 9" stroke="#0054CE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="hidden" id="faq-<?= $chave ?>">
                                    <?= $faq['texto'] ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </section>
<?php get_footer() ?>
