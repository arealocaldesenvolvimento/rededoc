<?php get_header() ?>

    <section class="slider">
        <?= do_shortcode('[rev_slider alias="Home"][/rev_slider]') ?>
    </section>
    <section class="diretrizes">
        <div class="al-container">
            <div class="listagem">
                <article class="diretriz">
                    <div class="left">
                        <img src="<?= get_image_url('acessivel.png')?>" alt="Ícone Acessibilidade">
                    </div>
                    <div class="right">
                        <h2>Acessibilidade</h2>
                        <div class="text">
                            <?= get_field("acessibilidade") ?>
                        </div>
                    </div>
                </article>
                <article class="diretriz">
                    <div class="left">
                        <img src="<?= get_image_url('disponibilidade.png')?>" alt="Ícone Disponibilidade">
                    </div>
                    <div class="right">
                        <h2>Disponibilidade</h2>
                        <div class="text">
                            <?= get_field("disponibilidade") ?>
                        </div>
                    </div>
                </article>
                <article class="diretriz">
                    <div class="left">
                        <img src="<?= get_image_url('popular.png')?>" alt="Ícone Popularidade">
                    </div>
                    <div class="right">
                        <h2>Popular</h2>
                        <div class="text">
                            <?= get_field("popularidade") ?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
    <section class="especialidades">
        <div class="al-container">
            <div class="right-content">
                <div class="lista">
                    <?php
                        $especialidades = new WP_Query(array(
                            'post_type' => 'especialidade',
                            'posts_per_page'	=> -1,
                            'orderby'        => 'title',
                            'order'          => 'ASC'
                        ));
                        while($especialidades->have_posts()):
                            $especialidades->the_post();
                            ?>
                            <a href="<?= get_permalink()?>" class="child"><div class="contain"><?= the_title() ?></div></a>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
    <section class="servicos" id="servicos">
        <div class="r-container">
            <div class="left-right">
                <h2>Conheça nossos<br><span>serviços</span></h2>
                <div class="contain">
                    <div class="left">
                        <?php
                            $servicos = new WP_Query(array(
                                'post_type' => 'servico',
                                'posts_per_page'	=> -1
                            ));
                            while($servicos->have_posts()):
                                $servicos->the_post();
                                ?>
                                <a href="<?= get_permalink()?>" class="servico">
                                    <div class="algum-container">
                                        <div class="left-image">
                                            <img src="<?= get_field("imagem_thumb")['url'] ?>" alt="Imagem Thumbnail de serviço">
                                        </div>
                                        <div class="right-text">
                                            <span class="nome"><?= get_field("servicos_descricao")?></span><br>
                                            <span class="saiba-mais">Saiba Mais 
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0_24_270)">
                                                    <path d="M7.58335 1.75L12.8334 7L7.58335 12.25M1.16669 7H12.8334H1.16669Z" stroke="#2E82E1" stroke-width="2"/>
                                                </g>
                                                <defs>
                                                    <clipPath id="clip0_24_270">
                                                        <rect width="14" height="14" fill="white"/>
                                                    </clipPath>
                                                </defs>
                                            </svg>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                        <?php endwhile; ?>
                    </div>
                    <div class="right">
                        <img src="<?= get_image_url("servicos.png") ?>" alt="Imagem Serviços">
                    </div>
                </div>
                <div class="agendar">
                    <a href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")['back'] ?>" target="_blank" class="whatsapp">
                        <div class="organizar">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_30_10)">
                                    <path d="M27.202 4.6521C25.7349 3.17145 23.9879 1.99759 22.0626 1.19891C20.1373 0.40023 18.0723 -0.00728855 15.988 9.86611e-05C7.254 9.86611e-05 0.136 7.1161 0.128 15.8521C0.128 18.6501 0.86 21.3721 2.242 23.7821L0 32.0001L8.408 29.7961C10.734 31.0619 13.3399 31.7254 15.988 31.7261H15.996C24.732 31.7261 31.848 24.6101 31.856 15.8661C31.858 13.7822 31.4476 11.7185 30.6485 9.79386C29.8494 7.86924 28.6774 6.12176 27.2 4.6521H27.202ZM15.988 29.0421C13.626 29.0429 11.3074 28.4073 9.276 27.2021L8.796 26.9141L3.808 28.2221L5.14 23.3561L4.828 22.8541C3.50761 20.7547 2.80929 18.3242 2.814 15.8441C2.814 8.5921 8.728 2.6761 15.996 2.6761C17.7275 2.67299 19.4425 3.01265 21.042 3.67548C22.6416 4.33832 24.0942 5.31122 25.316 6.5381C26.5421 7.7602 27.514 9.21295 28.1758 10.8126C28.8377 12.4122 29.1762 14.127 29.172 15.8581C29.164 23.1361 23.25 29.0421 15.988 29.0421ZM23.218 19.1741C22.824 18.9761 20.878 18.0181 20.512 17.8821C20.148 17.7521 19.882 17.6841 19.622 18.0801C19.356 18.4741 18.596 19.3721 18.368 19.6301C18.14 19.8961 17.904 19.9261 17.508 19.7301C17.114 19.5301 15.836 19.1141 14.324 17.7601C13.144 16.7101 12.354 15.4101 12.118 15.0161C11.89 14.6201 12.096 14.4081 12.294 14.2101C12.468 14.0341 12.688 13.7461 12.886 13.5181C13.086 13.2901 13.152 13.1221 13.282 12.8581C13.412 12.5901 13.35 12.3621 13.252 12.1641C13.152 11.9661 12.362 10.0121 12.028 9.2241C11.708 8.4461 11.382 8.5541 11.138 8.5441C10.91 8.5301 10.644 8.5301 10.378 8.5301C10.1771 8.53509 9.97945 8.58155 9.79739 8.66656C9.61532 8.75157 9.45279 8.8733 9.32 9.0241C8.956 9.4201 7.938 10.3781 7.938 12.3321C7.938 14.2861 9.358 16.1641 9.558 16.4301C9.754 16.6961 12.346 20.6941 16.324 22.4141C17.264 22.8241 18.004 23.0661 18.582 23.2501C19.532 23.5541 20.39 23.5081 21.074 23.4101C21.834 23.2941 23.416 22.4501 23.75 21.5241C24.078 20.5961 24.078 19.8041 23.978 19.6381C23.88 19.4701 23.614 19.3721 23.218 19.1741Z" fill="white"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0_30_10">
                                        <rect width="32" height="32" fill="white"/>
                                    </clipPath>
                                </defs>
                            </svg>
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_489_2)">
                                    <path d="M27.202 4.6521C25.7349 3.17145 23.9879 1.99759 22.0626 1.19891C20.1373 0.40023 18.0723 -0.00728855 15.988 9.86611e-05C7.254 9.86611e-05 0.136 7.1161 0.128 15.8521C0.128 18.6501 0.86 21.3721 2.242 23.7821L0 32.0001L8.408 29.7961C10.734 31.0619 13.3399 31.7254 15.988 31.7261H15.996C24.732 31.7261 31.848 24.6101 31.856 15.8661C31.858 13.7822 31.4476 11.7185 30.6485 9.79386C29.8494 7.86924 28.6774 6.12176 27.2 4.6521H27.202ZM15.988 29.0421C13.626 29.0429 11.3074 28.4073 9.276 27.2021L8.796 26.9141L3.808 28.2221L5.14 23.3561L4.828 22.8541C3.50761 20.7547 2.80929 18.3242 2.814 15.8441C2.814 8.5921 8.728 2.6761 15.996 2.6761C17.7275 2.67299 19.4425 3.01265 21.042 3.67548C22.6416 4.33832 24.0942 5.31122 25.316 6.5381C26.5421 7.7602 27.514 9.21295 28.1758 10.8126C28.8377 12.4122 29.1762 14.127 29.172 15.8581C29.164 23.1361 23.25 29.0421 15.988 29.0421V29.0421ZM23.218 19.1741C22.824 18.9761 20.878 18.0181 20.512 17.8821C20.148 17.7521 19.882 17.6841 19.622 18.0801C19.356 18.4741 18.596 19.3721 18.368 19.6301C18.14 19.8961 17.904 19.9261 17.508 19.7301C17.114 19.5301 15.836 19.1141 14.324 17.7601C13.144 16.7101 12.354 15.4101 12.118 15.0161C11.89 14.6201 12.096 14.4081 12.294 14.2101C12.468 14.0341 12.688 13.7461 12.886 13.5181C13.086 13.2901 13.152 13.1221 13.282 12.8581C13.412 12.5901 13.35 12.3621 13.252 12.1641C13.152 11.9661 12.362 10.0121 12.028 9.2241C11.708 8.4461 11.382 8.5541 11.138 8.5441C10.91 8.5301 10.644 8.5301 10.378 8.5301C10.1771 8.53509 9.97945 8.58155 9.79739 8.66656C9.61532 8.75157 9.45279 8.8733 9.32 9.0241C8.956 9.4201 7.938 10.3781 7.938 12.3321C7.938 14.2861 9.358 16.1641 9.558 16.4301C9.754 16.6961 12.346 20.6941 16.324 22.4141C17.264 22.8241 18.004 23.0661 18.582 23.2501C19.532 23.5541 20.39 23.5081 21.074 23.4101C21.834 23.2941 23.416 22.4501 23.75 21.5241C24.078 20.5961 24.078 19.8041 23.978 19.6381C23.88 19.4701 23.614 19.3721 23.218 19.1741V19.1741Z" fill="#52A844"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0_489_2">
                                        <rect width="32" height="32" fill="white"/>
                                    </clipPath>
                                </defs>
                            </svg>
                            <span>Agendar Agora</span>
                        </div>
                    </a>
                    <span>ou ligue <a href="tel:<?= get_field("telefone", "option")['back']?>"><?= get_field("telefone", "option")['front']?></a></span>
                </div>
            </div>
        </div>
    </section>
    <section class="doc-ocupacional">
        <div class="al-container">
            <h2>Conheça a<br><span>DOC OCUPACIONAL</span></h2> 
            <div class="doc-ocpacional-lista">
                <?php
                    $docs = new WP_Query(array(
                        'post_type' => 'doc-ocupacional',
                        'posts_per_page'	=> -1
                    ));
                    $count = 0;
                        while($docs->have_posts()):
                            $docs->the_post();
                            ?>
                            <a href="<?= get_permalink() ?>"><div class="background-color-<?= $count ?>"><?= the_title() ?></div></a>
                <?php 
                    $count++;
                    endwhile; 
                ?>
            </div> 
            <div class="saiba-mais">
                <a href="<?= get_home_url()?>/doc-ocupacional"><div>Saiba Mais!</div></a>
            </div>
        </div>
    </section>
    <section class="news-letter margemfds">
        <div class="al-container">
            <div class="title correcao">
                <span>Fique por dentro das</span>
                <h2>DICAS DE SAÚDE</h2>
                <p>Conteúdo descomplicado de saúde e bem-estar. Acompanhe nosso <a href="<?= get_home_url() ?>/blog">blog</a>!</p>
            </div>
            <div class="blog-lista">
                <ul>
                    <?php
                        $posts = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'	=> 15,
                            'orderby'        => 'date',
                            'order'          => 'DESC'
                        ));
                        while($posts->have_posts()):
                            $posts->the_post();
                            ?>
                            <li>
                                <a href="<?= get_permalink()?>">
                                    <div class="blog-single home">
                                        <div class="img">
                                            <img src="<?= get_the_post_thumbnail_url()!="" ? get_the_post_thumbnail_url() : get_image_url("indisponivel.png") ?>" alt="Thumbnail do blog">
                                        </div>
                                        <div class="text">
                                            <h2><?= the_title()?></h2>
                                            <h3><?= get_the_date()?></h3>
                                        </div>
                                    </div>
                                </a>
                            </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="forma-de-mudar">
        <div class="al-container">
            <div class="contain">
                <div class="left">
                    <span>Conheça nossa forma</span>
                    <h2>forma de mudar o mundo</h2>
                    <div class="text">Impactamos quantas vidas forem necessárias para fazer a diferença, com foco no cuidado, acolhimento e experiência do paciente.</div>
                </div>
                <div class="right">
                    <a href="">Conheça a rede DOC</a>
                </div>
            </div>
        </div>
    </section>
    <section class="news-letter">
        <div class="al-container">
            <div class="title">
                <span>Assine nossa</span>
                <h2>newsletter</h2>
                <p>Receba conteúdo com qualidade e dicas sobre saúde
                preenchendo seus dados abaixo:</p>
            </div>
            <div class="shortcode"><?= do_shortcode("[newsletter_form]") ?></div>
        </div>
    </section>
<?php get_footer() ?>
