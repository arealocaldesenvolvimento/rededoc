<?php get_header() ?>
    <div class="al-container">
        <div class="local"><a href="<?= get_home_url()?>">Home</a>  <span>|</span>  <a href="<?= get_home_url()?>/blog">Blog</a>   <span>|</span>  <?= the_title() ?></div>
    </div>
    <section class="blog">
        <div class="al-container">
            <h2 class="title"><?= the_title() ?></h2>
            <h2 class="date"><?= get_the_date() ?></h2>
            <div class="thumb">
                <img src="<?= get_the_post_thumbnail_url()!="" ? get_the_post_thumbnail_url() : get_image_url("indisponivel.png") ?>" alt="Thumbnail do blog">
            </div>
            <div class="text"><?= the_content() ?></div>
        </div>
        <div class="hr"></div>
        <div class="al-container">
            <div class="share">Compartilhe <div class="shortc"><a id="copy" link="<?= get_permalink() ?>"><img src="<?= get_image_url("link-share.png") ?>" alt=""></a> <?= do_shortcode('[addtoany]') ?></div></div>
        </div>
    </section>
    <section class="interese">
        <div class="al-container">
            <h2 class="subtitle">Talvez você se</h2>
            <h2 class="subtitle-2">interesse</h2>
        </div>
        <div class="al-container contain">
            <?php
                $posts = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page'	=> 3,
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'post__not_in' => array( get_the_ID() ) 
                ));
                while($posts->have_posts()):
                    $posts->the_post();
                    ?>
                    <a href="<?= get_permalink()?>">
                        <div class="blog-single">
                            <div class="img">
                                <img src="<?= get_the_post_thumbnail_url()!="" ? get_the_post_thumbnail_url() : get_image_url("indisponivel.png") ?>" alt="Thumbnail do blog">
                            </div>
                            <div class="text">
                                <h2><?= the_title()?></h2>
                                <h3><?= get_the_date()?></h3>
                            </div>
                        </div>
                    </a>
            <?php endwhile; ?>
        </div>
    </section>
    <section class="news-letter">
        <div class="al-container">
            <div class="title">
                <span>Assine nossa</span>
                <h2>newsletter</h2>
                <p>Receba conteúdo com qualidade e dicas sobre saúde
                preenchendo seus dados abaixo:</p>
            </div>
            <div class="shortcode"><?= do_shortcode("[newsletter_form]") ?></div>
        </div>
    </section>
<?php get_footer() ?>