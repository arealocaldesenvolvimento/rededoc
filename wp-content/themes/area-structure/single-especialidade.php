<?php get_header() ?>
    <section class="thumb-page" style="background-image: url(<?= get_image_url("especialidade-banner.png") ?>);">
        <div class="al-container">
            <div class="local"><a href="<?= get_home_url()?>">Home</a>  <span>|</span>  <a href="<?= get_home_url()?>/especialidades">Especialidades</a>   <span>|</span>  <?= the_title() ?></div>
            <div class="title"><?= the_title() ?></div>
        </div>
    </section>
    <section class="single-especialidade">
        <div class="al-container">
            <div class="texto-top">
                <?= get_field("texto_top") ?>
            </div>
            <div class="right">
                <div class="consulta">
                    <h2>Agende uma consulta!</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut magna faucibus,</p>
                    <div class="f">
                        <div class="preco">
                            <div class="con">Consultas <br> a partir de:</div><h2><small class="first">R$</small><?= explode(".", get_field('valor'))[0] ?><small class="second">,<?= explode(".", get_field('valor'))[1] ?></small></h2>
                        </div>
                        <div>
                            <a class="revelar" id="preco-<?= mb_strtolower(str_replace(" ", "-", get_the_title())); ?>">ver preço da consulta</a>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <a href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")['back'] ?>" target="_blank" class="whatsapp">
                            <div class="organizar">
                                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.7014 3.19826C17.6928 2.18031 16.4917 1.37328 15.168 0.824189C13.8444 0.275097 12.4247 -0.00507191 10.9917 6.79433e-06C4.98712 6.79433e-06 0.0935 4.89226 0.088 10.8983C0.088 12.8219 0.59125 14.6933 1.54138 16.3501L0 22L5.7805 20.4848C7.37963 21.355 9.17115 21.8112 10.9917 21.8116H10.9973C17.0033 21.8116 21.8955 16.9194 21.901 10.9079C21.9024 9.47519 21.6202 8.05639 21.0709 6.73321C20.5215 5.41004 19.7157 4.20865 18.7 3.19826H18.7014ZM10.9917 19.9664C9.36787 19.9669 7.77384 19.5299 6.37725 18.7014L6.04725 18.5034L2.618 19.4026L3.53375 16.0573L3.31925 15.7121C2.41148 14.2688 1.93139 12.5978 1.93462 10.8928C1.93462 5.90701 6.0005 1.83976 10.9973 1.83976C12.1876 1.83762 13.3667 2.07113 14.4664 2.52683C15.5661 2.98253 16.5648 3.6514 17.4048 4.49488C18.2477 5.33507 18.9159 6.33384 19.3709 7.43357C19.8259 8.53331 20.0587 9.71224 20.0557 10.9024C20.0503 15.906 15.9844 19.9664 10.9917 19.9664ZM15.9624 13.1821C15.6915 13.046 14.3536 12.3874 14.102 12.2939C13.8517 12.2045 13.6689 12.1578 13.4901 12.43C13.3073 12.7009 12.7847 13.3183 12.628 13.4956C12.4712 13.6785 12.309 13.6991 12.0367 13.5644C11.7659 13.4269 10.8872 13.1409 9.84775 12.21C9.0365 11.4881 8.49337 10.5944 8.33113 10.3235C8.17437 10.0513 8.316 9.90551 8.45212 9.76938C8.57175 9.64838 8.723 9.45038 8.85913 9.29363C8.99662 9.13688 9.042 9.02138 9.13137 8.83988C9.22075 8.65563 9.17812 8.49888 9.11075 8.36276C9.042 8.22663 8.49887 6.88326 8.26925 6.34151C8.04925 5.80663 7.82512 5.88088 7.65737 5.87401C7.50062 5.86438 7.31775 5.86438 7.13488 5.86438C6.99678 5.86781 6.86088 5.89976 6.7357 5.9582C6.61053 6.01664 6.49879 6.10033 6.4075 6.20401C6.15725 6.47626 5.45738 7.13488 5.45738 8.47826C5.45738 9.82163 6.43362 11.1128 6.57112 11.2956C6.70587 11.4785 8.48787 14.2271 11.2227 15.4096C11.869 15.6915 12.3778 15.8579 12.7751 15.9844C13.4283 16.1934 14.0181 16.1618 14.4884 16.0944C15.0109 16.0146 16.0985 15.4344 16.3281 14.7978C16.5536 14.1598 16.5536 13.6153 16.4849 13.5011C16.4175 13.3856 16.2346 13.3183 15.9624 13.1821Z" fill="white"/>
                                </svg>
                                <span>Agendar Agora</span>
                            </div>
                        </a><br>
                        <span class="liga">ou ligue <a href="tel:<?= get_field("telefone", "option")['back']?>"><?= get_field("telefone", "option")['front']?></a></span>
                    </div>
                    <hr>
                    <p class="dif">Se preferir, entramos em contato com você!</p>
                    <div class="short">
                        <?= do_shortcode('[contact-form-7 id="203" title="Entrar em contato"]'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding"></div>
    </section>
<?php get_footer() ?>