function abrir(id){
	$("#faq-"+id).slideDown();
	$("#up-"+id).show();
	$("#down-"+id).hide();
	$("#line-"+id).css("padding-bottom", "14px");
}
function fechar(id){
	$("#faq-"+id).slideUp();
	$("#up-"+id).hide();
	$("#down-"+id).show();
	$("#line-"+id).css("padding-bottom", "22px");
}
function auto(id){
	if(!$("#faq-"+id).is(":visible")){
		abrir(id);
	}else{
		fechar(id);
	}
}
function abrirBot(id){
	$("#faq-bot-"+id).slideDown();
	$("#up-bot-"+id).show();
	$("#down-bot-"+id).hide();
	$("#line-bot"+id).css("padding-bottom", "14px");
}
function fecharBot(id){
	$("#faq-bot-"+id).slideUp();
	$("#up-bot-"+id).hide();
	$("#down-bot-"+id).show();
	$("#line-bot-"+id).css("padding-bottom", "22px");
}
function autoBot(id){
	if(!$("#faq-bot-"+id).is(":visible")){
		abrirBot(id);
	}else{
		fecharBot(id);
	}
}