/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}
window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
	if(document.querySelector(".tnp-name")!=null){
		document.querySelector(".tnp-name").placeholder="Como gostaria de ser chamado?"
	}
	if(document.querySelector(".tnp-email")!=null){
		document.querySelector(".tnp-email").placeholder="Digite seu melhor e-mail"
	}
	$(".hidden").hide();
	$(".up").hide();
	$(".wpcf7-submit").on("click", ()=>{
		$(".wpcf7-spinner").css("display", "inline-block");
	});
	if(document.querySelector("#copy")){
		document.querySelector("#copy").addEventListener("click", ()=>{
			var TextToCopy = document.querySelector("#copy").attributes.link.value;
			var TempText = document.createElement("input");
			TempText.value = TextToCopy;

			document.body.appendChild(TempText);
			TempText.select();
			
			document.execCommand("copy");
			document.body.removeChild(TempText);
		});
	}
	$('.blog-lista ul').lightSlider({
        item: 3,
        loop: false,
        slideMove: 2,
        speed: 1000,
        prevHtml: "<img src='http://rededocmais.com.br/wp-content/themes/area-structure/assets/images/prev.png'>",
		nextHtml: "<img src='http://rededocmais.com.br/wp-content/themes/area-structure/assets/images/next.png'>",
		responsive : [
            {
                breakpoint:1100,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:780,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]

    }); 
	$(".preco").hide();
	$(".revelar").on("click", ()=>{
		console.log("teste")
		$(".revelar").parent().css("display", "none");
		$(".preco").show( "slow");
		$(".preco").css("display", "flex");
	});
}
$(document).ready(()=>{
	$(".wpcf7-tel").mask('(00) 00000-0000');
	$(".cnpj input").mask('00.000.000/0000-00', {reverse: true});
	setTimeout(() => {
		$(".lSPager li a").css("background-color", "#E1E1E1");
		$(".lSPager .active a").css("background-color", "#2E82E1");
		$(".lSPager li").on("click", ()=>{
			$(".lSPager li a").css("background-color", "#E1E1E1");
			$(".lSPager .active a").css("background-color", "#2E82E1");
		});
		$(".lSAction a").on("click", ()=>{
			$(".lSPager li a").css("background-color", "#E1E1E1");
			$(".lSPager .active a").css("background-color", "#2E82E1");
		});
		$(".lightSlider li").hover(()=>{
			$(".lSPager li a").css("background-color", "#E1E1E1");
			$(".lSPager .active a").css("background-color", "#2E82E1");
		});
	}, "3000");
	$(".lSPager li").on("click", ()=>{
		$(".lSPager li a").css("background-color", "#E1E1E1");
		$(".lSPager .active a").css("background-color", "#2E82E1");
	});
	$(".lSAction a").on("click", ()=>{
		$(".lSPager li a").css("background-color", "#E1E1E1");
		$(".lSPager .active a").css("background-color", "#2E82E1");
	});
	$(".lightSlider li").hover(()=>{
		$(".lSPager li a").css("background-color", "#E1E1E1");
		$(".lSPager .active a").css("background-color", "#2E82E1");
	});
});