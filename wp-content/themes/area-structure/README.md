# AreaStructure 4.0 - Tema personalizado

![alt](screenshot.jpg)

- Antes de iniciar um novo projeto remova o diretório oculto ".git" em estrutura-basica/

- Compilar assets e/ou minificá-los (NPM & Yarn):
```language
npm i
npm run watch
npm run production
```
```language
yarn
yarn watch
yarn production
```
