<?php

flush_rewrite_rules();

/**
 * Example post-type.
 */
function especialidadePost(): void
{
    register_post_type('especialidade', [
        'labels' => [
            'name' => _x('Especialidades', 'especialidades'),
            'singular_name' => _x('Especialidade', 'especialidade'),
            'add_new' => __('Adicionar novo'),
            'add_new_item' => __('Adicionar novo'),
            'edit_item' => __('Editar'),
            'new_item' => __('Novo'),
            'view_item' => __('Ver'),
            'not_found' => __('Nada encontrado'),
            'not_found_in_trash' => __('Nada encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Especialidades',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
add_action('init', 'especialidadePost');

function servicosPost(): void
{
    register_post_type('servico', [
        'labels' => [
            'name' => _x('Serviços', 'serviços'),
            'singular_name' => _x('Serviço', 'serviço'),
            'add_new' => __('Adicionar novo'),
            'add_new_item' => __('Adicionar novo'),
            'edit_item' => __('Editar'),
            'new_item' => __('Novo'),
            'view_item' => __('Ver'),
            'not_found' => __('Nada encontrado'),
            'not_found_in_trash' => __('Nada encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Serviços',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
add_action('init', 'servicosPost');

function docOcupacionalPost(): void
{
    register_post_type('doc-ocupacional', [
        'labels' => [
            'name' => _x('Doc Ocupacionais', 'doc ocupacionais'),
            'singular_name' => _x('Doc Ocupacional', 'doc ocupacional'),
            'add_new' => __('Adicionar novo'),
            'add_new_item' => __('Adicionar novo'),
            'edit_item' => __('Editar'),
            'new_item' => __('Novo'),
            'view_item' => __('Ver'),
            'not_found' => __('Nada encontrado'),
            'not_found_in_trash' => __('Nada encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Doc Ocupacionais',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
add_action('init', 'docOcupacionalPost');