<?php get_header() ?>
    <section class="thumb-page" style="background-image: url(<?= get_image_url("blog-banner.png") ?>);">
        <div class="al-container">
            <div class="local"><a href="<?= get_home_url() ?>">Home</a>  <span>|</span>  Blog</div>
            <div class="title">Blog</div>
        </div>
    </section>
    <section class="blog">
        <div class="al-container contain">
            <?php
            $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
                $posts = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page'	=> 12,
                    'orderby'        => 'date',
                    'order'          => 'DESC',
                    'paged'  => $paged
                ));
                while($posts->have_posts()):
                    $posts->the_post();
                    ?>
                    <a href="<?= get_permalink()?>">
                        <div class="blog-single">
                            <div class="img">
                                <img src="<?= get_the_post_thumbnail_url()!="" ? get_the_post_thumbnail_url() : get_image_url("indisponivel.png") ?>" alt="Thumbnail do blog">
                            </div>
                            <div class="text">
                                <h2><?= the_title()?></h2>
                                <h3><?= get_the_date()?></h3>
                            </div>
                        </div>
                    </a>
            <?php endwhile; ?>
        </div>
        <div class="paginacao">
            <?= paginationLinks($posts) ?>
        </div>
    </section>
<?php get_footer() ?>