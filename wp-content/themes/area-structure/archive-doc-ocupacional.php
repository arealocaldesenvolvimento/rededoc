<?php get_header() ?>
    <section class="thumb-page" style="background-image: url(<?= get_image_url("doc-ocupacional-banner.png") ?>);">
        <div class="al-container">
            <div class="local"><a href="<?= get_home_url()?>">Home</a>  <span>|</span>  <a href="<?= get_home_url()?>/doc-ocupacional">Doc Ocupacional</a></div>
            <div class="title">Doc Ocupacional</div>
        </div>
    </section>
    <div class="doc-texto">
        <div class="al-container">
            <div class="center">
                O grupo Rede Doc iniciou suas atividades inicialmente com o objetivo de prestar atendimento às empresas a nível ambulatorial em medicina ocupacional e medicina preventiva, e graças à confiança dos empresários locais e da população atendida, bem como a qualidade dos serviços oferecidos desde 2019, teve uma potencial expansão e em 2021 startou o seu Hub de soluções em saúde e conta agora com a D.OC ocupacional como uma de suas mais novas soluções
            </div>
            <div class="left-right">
                <div class="left">
                    O grupo Rede Doc iniciou suas atividades inicialmente com o objetivo de prestar atendimento às empresas a nível ambulatorial em medicina ocupacional e medicina preventiva, e graças à confiança dos empresários locais e da população atendida, bem como a qualidade dos serviços oferecidos desde 2019, teve uma potencial expansão e em 2021 startou o seu Hub de soluções em saúde e conta agora com a D.OC ocupacional como uma de suas mais novas soluções
                </div>
                <div class="right">
                    Dispomos de equipe técnica com profissionais Engenheiros e Técnicos de Segurança do Trabalho; Médicos do Trabalho; Técnicos de Enfermagem e Enfermeiros do Trabalho; Fisioterapeutas do Trabalho, além de equipe de suporte administrativo, capacitada para oferecer atendimento personalizado e de qualidade aos nossos clientes.
                </div>
            </div>
            <div class="left-right-img">
                <div class="left">
                    A D.OC ocupacional entendeu que as empresas de Rio do Sul e do Alto Vale do Itajaí necessitam de uma empresa séria e de confiança na Engenharia e segurança do trabalho e por isso nossa empresa possui todos os registros nos orgãos competentes com profissionais competentes e habilitados tecnicamente a prestarem o serviço de excelência da forma que o Grupo Rede Doc acredita que deva ser , encantando os nossos clientes e lhes fornecendo a melhor experiência de usuário.
                </div>
                <div class="right">
                    <img src="<?= get_image_url("img-doc.png") ?>" alt="Banner lateral">
                </div>
            </div>
            <div class="left">
                Acreditar no incremento da produtividade por meio da qualidade de vida dos
                colaboradores de empresas de todo o país. Com esta visão inovadora,a Gestora de saúde Camila Laurindo e a administradora de empresas Tainá Laurindo fundaram em 2019 a Rede Doc. Com o propósito pioneiro de oferecer soluções completas em saúde empresarial, o Grupo Rede Doc apostou na sua consolidação e, assim, iniciou a sua trajetória de êxito.propondo a segmentação de seus serviços em quatro unidades de negócios: Gestão de Serviços, Saúde Ocupacional, Cursos e Treinamentos e Qualidade de Vida.<br><br>
                Possuímos software de gestão ocupacional que faz a geração e envio dos eventos do eSocial, diretamente ao ambiente e Social do Governo Federal. Assim, os Eventos de Saúde e Segurança Ocupacional exigidos pelo eSocial são transmitidos com agilidade e confiabilidade. É muito mais segurança para as informações digitais de sua empresa.
            </div>
        </div>
    </div>
    <section class="doc-ocupacional">
        <div class="al-container">
            <div class="doc-ocpacional-lista">
                <?php
                    $docs = new WP_Query(array(
                        'post_type' => 'doc-ocupacional',
                        'posts_per_page'	=> -1
                    ));
                    $count = 0;
                        while($docs->have_posts()):
                            $docs->the_post();
                            ?>
                            <a href="<?= get_permalink() ?>"><div class="background-color-<?= $count ?>"><?= the_title() ?></div></a>
                <?php 
                    $count++;
                    endwhile; 
                ?>
            </div> 
        </div>
    </section>
<?php get_footer() ?>